import pandas as pd
import numpy as np
import os
import glob
import re
from random import shuffle
from collections import Counter
import warnings
import xlsxwriter
from functools import reduce
from statistics import mean
import math
import openpyxl
import sys
pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
np.seterr(all='raise')

def skill_matrices(df):
    # for ratee in df['ratee'].unique():
    #     if not ratee in df['rater'].unique():
    #         print('\n')
    #         print(ratee, ' _is not in rater', )
    #
    # print('\n\n\n')
    # for rater in df['rater'].unique():
    #     if not rater in df['ratee'].unique():
    #         print('\n')
    #         print(rater, ' _is not in ratee', )

    # return
    # print(df)
    # print(df['ratee'].unique())
    # print(df['rater'].unique())
    #
    # print(set(df['ratee'].unique())==set(df['rater'].unique()))
    workbook = xlsxwriter.Workbook('peer.xlsx', {'nan_inf_to_errors': True})

    df.reset_index(inplace=True, drop=True)
    fmt = workbook.add_format({"font_name": "Garamond"})
    left_top_fmt = workbook.add_format({"font_name": "Garamond", 'left': 1, 'top': 1})
    top_bold_fmt_left_right = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'left': 1, 'right': 1})
    top_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1})
    top_right_bold_center = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'right': 1, 'left': 1})
    left_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'left': 1, 'bottom': 1})
    right_bottom_bold_center_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'bottom': 1})
    left_fmt = workbook.add_format({"font_name": "Garamond", 'left': 1})
    left_right_fmt = workbook.add_format({"font_name": "Garamond", 'right': 1, 'left': 1})
    bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True})
    center_bold_fmt_left_right = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'bottom': 1})
    center_bold_fmt_bottom = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'bottom': 1})
    center_bold = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1})
    center_bold_top = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'left': 1, 'right': 1, 'top': 1, 'bottom': 1})

    Frames = []

    for ratee in df['ratee'].unique():
        # if ratee in df['rater'].unique():
            # print(ratee)
        for skill in df[df['ratee']==ratee]['skill'].unique():
            # print('here')
            rateeFrame = df[(df['ratee']==ratee) & (df['skill']==skill)]
            rateeFrame['othersRatingOfMeAveraged'] = df[(df['rater']!=ratee) & (df['ratee']==ratee)  & (df['skill']==skill)]['rating'].mean()
            rateeFrame['boardAverageOfSkill'] = df[df['skill']==skill]['rating'].mean()
            # print(rateeFrame)
            Frames.append(rateeFrame)

    finalFrame = pd.concat(Frames)
    # print(finalFrame)
    for ratee in finalFrame['ratee'].unique():
        # if ratee in finalFrame['rater'].unique():
        # try:
        len_of_rater_df = len(finalFrame[finalFrame['ratee'] == ratee])
        ratee_df = finalFrame[(finalFrame['ratee'] == ratee) & (finalFrame['rater']==ratee)]
        ratee_df = ratee_df.sort_values(['rating', 'othersRatingOfMeAveraged', 'boardAverageOfSkill'], ascending=False)
        # print(rater_df)
        worksheet = workbook.add_worksheet(str(ratee[0:30]))
        worksheet.merge_range(1, 1, 1, 4, ratee, top_right_bold_center)
        worksheet.write(2, 1, 'Skill Areas:', left_bold_fmt)
        worksheet.write(2, 2, 'Self-Rating', center_bold_fmt_bottom)
        worksheet.write(2, 3, 'Peer Rating Average', center_bold_fmt_bottom)
        worksheet.write(2, 4, 'Board Overall Average.', right_bottom_bold_center_fmt)

        ten_format = workbook.add_format({'bg_color': '#63be7b'})
        nine_format = workbook.add_format({'bg_color': '#86c97e'})
        eight_format = workbook.add_format({'bg_color': '#a9d27f'})
        seven_format = workbook.add_format({'bg_color': '#ccdd82'})
        six_format = workbook.add_format({'bg_color': '#eee683'})
        five_format = workbook.add_format({'bg_color': '#fedd81'})
        four_format = workbook.add_format({'bg_color': '#fcbf7b'})
        three_format = workbook.add_format({'bg_color': '#fba276'})
        two_format = workbook.add_format({'bg_color': '#f98570'})
        one_format = workbook.add_format({'bg_color': '#f8696b'})

        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '9.01',
        'maximum': '10',
        'format': ten_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '8.01',
        'maximum': '9',
        'format': nine_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '7.01',
        'maximum': '8',
        'format': eight_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '6.01',
        'maximum': '7',
        'format': seven_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '5.01',
        'maximum': '6',
        'format': six_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '4.01',
        'maximum': '5',
        'format': five_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '3.01',
        'maximum': '4',
        'format': four_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '2.01',
        'maximum': '3',
        'format': three_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '1.01',
        'maximum': '2',
        'format': two_format})
        worksheet.conditional_format(3, 2, len_of_rater_df+3, 4,
        {'type': 'cell',
        'criteria': 'between',
        'minimum': '0.01',
        'maximum': '1',
        'format': one_format})

        worksheet.write_column(3, 1, ratee_df['skill'].unique().tolist(), left_fmt)
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 2, ratee_df['rating'].tolist()[beginRow-3], left_fmt)
            beginRow += 1
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 3, float('{:.2f}'.format(round(ratee_df['othersRatingOfMeAveraged'].tolist()[beginRow-3], 2))), left_fmt)
            beginRow += 1
        beginRow = 3
        for skill in ratee_df['skill'].unique():
            worksheet.write(beginRow, 4, float('{:.2f}'.format(round(ratee_df['boardAverageOfSkill'].tolist()[beginRow-3], 2))), left_right_fmt)
            beginRow += 1

        bold_align_right_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'right', 'left': 1, 'top': 1, 'bottom': 1})
        worksheet.write(len(ratee_df) + 3, 1, 'Average:', bold_align_right_fmt)
        worksheet.write(len(ratee_df)+3, 2, "{0:.2f}".format(ratee_df['rating'].mean()), center_bold_top)
        top_bold_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'top': 1, 'bottom': 1})
        top_right_bold_center_fmt = workbook.add_format({"font_name": "Garamond", 'bold': True, 'align': 'center', 'right': 1, 'top': 1, 'bottom': 1})

        worksheet.write(len(ratee_df)+3, 3, "{0:.2f}".format(ratee_df['othersRatingOfMeAveraged'].mean()), top_bold_fmt)
        worksheet.write(len(ratee_df)+3, 4, "{0:.2f}".format(ratee_df['boardAverageOfSkill'].mean()), top_right_bold_center_fmt)

    # except:
    #     print('Could not create skills matrix for {}'.format(ratee))

    worksheet = workbook.add_worksheet('Summary')
    worksheet.write_row(0, 0, finalFrame.columns)
    colNum = 0
    for col in finalFrame.columns:
        worksheet.write_column(1, colNum, finalFrame[col])
        colNum += 1
    workbook.close()

def parse():
    cwd = os.getcwd()
    extension = 'csv'
    skills_file = re.compile('^[^~].*csvExport.*\.{}'.format(extension), re.IGNORECASE)

    for file in os.listdir(cwd):
        if skills_file.match(file):
            print('\n-----------------\nProccessing Skills file...')
            print('Skills file name: ', file)
            df = pd.read_csv(file)

    columns = list(df.columns)
    columns = [re.sub(r'\xa0', ' ', c) for c in columns]
    df.columns = columns

    names_and_skills = re.compile('.*\d+\.\s+.*_.*_$')
    names_and_skills = list(filter(names_and_skills.match, columns))
    # print(names_and_skills)
    name_and_skill = []

    if 'MiddleName' in df.columns:
        raters = df['FirstName'].astype(str) + ' ' + df['MiddleName'].fillna('').astype(str) + ' ' + df['LastName'].astype(str)
    else:
        raters = df['FirstName'].astype(str) + ' ' + df['LastName'].astype(str)
    raters = raters.apply(lambda row: re.sub('\s+', ' ', row).strip())
    # if 'FullName' in df.columns:
    #     raters = df['FullName']
    # elif 'UniqueIdentifier' in df.columns:
    #     raters = df['UniqueIdentifier']


    dfs = []

    for n in names_and_skills:
        values = df[n]
        match = re.findall(".*\d+\.\s+(.*):\s*_(.*?)_", n)
        # match = re.search(".*\d+\.\s+(.*):\s*_(.*?)_", n)
        ratee = match[0][0]
        skill = match[0][1]
        fill = {'rater': raters.tolist(), 'ratee': [ratee] * len(raters), 'skill': [skill] * len(raters), 'rating': values}
        skill_matrix=pd.DataFrame(fill)
        dfs.append(skill_matrix)

    dfs = pd.concat(dfs)

    # writer = pd.ExcelWriter('test.xlsx')
    # dfs.to_excel(writer)
    # writer.save()

    return dfs

if __name__ == "__main__":
    print(sys.version)
    dfs = parse()
    skill_matrices(dfs)
